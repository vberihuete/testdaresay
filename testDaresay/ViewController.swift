//
//  ViewController.swift
//  testDaresay
//
//  Created by Vincent Berihuete on 2019-06-03.
//  Copyright © 2019 Vincent Berihuete. All rights reserved.
//

import UIKit
import Lottie

class ViewController: UIViewController {

    @IBOutlet weak var containerSV: UIStackView!
    let winkAnimationView =  AnimationView()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Event.screenView.track(with: ["screen" : String(describing: ViewController.self)])
        winkAnimationView.play { _ in
            Event.animationCompleted.track()
        }
    }

    @IBAction func buttonAction(_ sender: Any) {
        winkAnimationView.play{ _ in
            Event.animationCompleted.track()
        }
        Event.button.track()
    }
    
    
    /// sets the animation up
    private func setupAnimation() {
        winkAnimationView.animation = Animation.named("user_wink")
        winkAnimationView.loopMode = .playOnce
        containerSV.insertArrangedSubview(winkAnimationView, at: 1)
        winkAnimationView.heightAnchor.constraint(equalTo: winkAnimationView.widthAnchor, multiplier: 1.0/1.0).isActive = true
        winkAnimationView.translatesAutoresizingMaskIntoConstraints = false
    }
    
}

