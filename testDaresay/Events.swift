//
//  Events.swift
//  testDaresay
//
//  Created by Vincent Berihuete on 2019-06-03.
//  Copyright © 2019 Vincent Berihuete. All rights reserved.
//

import Foundation
import AppCenterAnalytics

enum Event: String {
    
    case button
    case screenView
    case animationCompleted
    
    static var dateFormatter: DateFormatter {
     let dt = DateFormatter()
        dt.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dt
    }
    
    func track(with parameters: [String: String] = [:]) {
        let params = addDate(to: parameters)
        MSAnalytics.trackEvent(self.rawValue, withProperties: params)
    }
    
    private func addDate(to parameters: [String: String]) -> [String: String] {
        var params = parameters
        params["date"] = Event.dateFormatter.string(from: Date())
        return params
    }
    
}
